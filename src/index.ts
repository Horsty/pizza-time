import * as sentences from './ressources/sentences.json';
import * as functions from './functions';

const getSentence = () => {
    const date: Date = new Date();
    const slotOfTime: string | undefined = functions.default.getSlotOfTime(date.getHours());
    const timeOfTheDay: string | undefined = functions.default.getTimeOfTheDay(date.getHours());
    const dayString: string = date.toLocaleString('en-us', { weekday: 'long' }).toLowerCase();
    const sentence: string = timeOfTheDay && dayString && slotOfTime ? sentences[dayString][timeOfTheDay][slotOfTime] : 'Pizza !!';

    if (sentence) {
        return sentence;
    }
}

const http = require('http');

const instructionsNouveauVisiteur = function (req, res) {

    res.writeHead(200, { "Content-Type": "text/html" });
    res.write(`<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8" />
            <title>C'est bientôt la pizza ?</title>
        </head> 
        <body>
         	<p style="text-align: center;margin-top: 200px;"><strong>${getSentence()}</strong></p>
        </body>
    </html>`);
    res.end();
}
const server = http.createServer(instructionsNouveauVisiteur);

server.listen(8080);