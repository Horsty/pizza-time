import * as slots from './ressources/slots.json';

export default {
    getSlotOfTime: function (hours: number): string | undefined {
        const timeOfTheDay = this.getTimeOfTheDay(hours);
        if (timeOfTheDay) {
            const start: number = slots.timeOfTheDay[timeOfTheDay].start;
            const end: number = slots.timeOfTheDay[timeOfTheDay].end;
            let difStart: number;
            let difEnd: number;

            difStart = hours >= start ? hours - start : start - hours;
            difEnd = hours < end ? end - hours : hours - end;

            if (difEnd === difStart) {
                return getSlot(slots.slotOfTime, 2);
            } else if (difEnd > difStart) {
                return getSlot(slots.slotOfTime, 1);
            }
            return getSlot(slots.slotOfTime, 3);
        }
        return getSlot(slots.slotOfTime, 2);
    },
    getTimeOfTheDay: function (hours: number): string | undefined {
        return getTime(slots.timeOfTheDay, hours);
    }
}

function getSlot(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

function getTime(object, value): string | undefined {
    return Object.keys(object).find((key: string) => {
        const start: number = object[key].start;
        const end: number = object[key].end;
        return compare(start, end, value);
    });
}

const compare = (start: number, end: number, value: number): boolean => {
    if (start > end) {
        return value > start || value < end || value == start
    } else {
        return value >= start && value < end
    }
}